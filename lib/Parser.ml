(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-37-39"]

module Base = struct

  type 'a res =
    | Val of 'a * char list
    | Err
  type 'a t = char list -> 'a  res

  let nyi = fun _ -> failwith "NYI"

  let return a = nyi

  let bind m f = nyi


end

module M = Monad.Expand (Base)
include M
open Base

let nyi = Base.nyi

let fail () = nyi

let any () = nyi

let empty () = nyi

let symbol c = nyi

let either m1 m2 = nyi

let optionally m = nyi

let rec star m = nyi
and plus m = nyi

let run m toks = failwith "NYI"


(* TODO: add a backtracking operator? *)
