module Make (S: sig 
                 type t
                 val init : t
               end) = struct

  module State = struct
    type 'a t = S.t -> 'a * S.t

    let return a = fun s -> (a,s)

    let bind (m:'a t) (f: 'a -> 'b t) : 'b t = fun s ->
      let (a,s') = m s in
      f a s'
  end

  module M = Monad.Expand (State)
  include M

  let get () s = (s,s)

  let set x _ =
    ((),x)
            
  let run m =
    fst (m S.init)

end
