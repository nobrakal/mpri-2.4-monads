open Monads

(* Taken from [https://wiki.haskell.org/State_Monad#Complete_and_Concrete_Example_1] *)

(* Passes a string of dictionary {a,b,c}
 * Game is to produce a number from the string.
 * By default the game is off, a 'c' toggles the
 * game on and off.
 * A 'a' gives +1 and a 'b' gives -1 when the game is on,
 * nothing otherwise.
 *)

module S = State.Make (struct type t = int
                                     let init = 0 end)
   
let ( let* ) = S.bind
let run = S.run

let playGame s =
  let rec aux run i =
    if i = String.length s
    then S.get ()
    else
      let* res = S.get () in
      match String.get s i with
      | 'c' ->
         aux (not run) (i+1)
      | _ ->
         if run
         then
           let res =
             match String.get s i with
             | 'a' -> res + 1
             | 'b' -> res - 1
             | _ -> res
           in
           let* () = S.set res in
           aux run (i+1)
         else
           aux run (i+1)
  in
  aux false 0

let result s = run (playGame s)

let%test _ = result "ab" = 0
let%test _ = result "ca" = 1
let%test _ = result "cabca" = 0
let%test _ = result "abcaaacbbcabbab" = 2
