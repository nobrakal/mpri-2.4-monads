open Monads.Nondeterminism

let rec insert (x:'a) (l:'a list) : 'a list t =
  match l with
  | [] -> return [x]
  | y::ys ->
     let* zs = insert x ys in
     let  zs = y::zs in
     either (return (x::l)) (return zs)

let rec permut (l:'a list) : 'a list t =
  match l with
  | [] -> return []
  | x::xs ->
     let* xs = permut xs in
     insert x xs

let%test _ = List.of_seq (all (permut [])) = [[]]
let%test _ = List.of_seq (all (permut [1])) = [[1]]
let%test _ = List.sort compare (List.of_seq (all (permut [1; 2]))) = [[1; 2]; [2; 1]]
let%test _ = List.sort compare (List.of_seq (all (permut [1; 2; 3]))) = [[1;2;3]; [1;3;2]; [2;1;3]; [2;3;1]; [3;1;2]; [3;2;1]]
