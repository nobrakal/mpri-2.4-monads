type value =
  | IsNat of int
  | IsBool of bool

type exp =
  | Val of value
  | Eq of exp * exp
  | Plus of exp * exp
  | Ifte of exp * exp * exp

let ( let* ) = Monads.Error.bind
let return = Monads.Error.return
let run = Monads.Error.run

exception WasBool
exception WasNat

let is_nat a =
  match a with
  | IsNat a -> return a
  | IsBool _ -> Monads.Error.err WasBool

let is_bool a =
  match a with
  | IsBool a -> return a
  | IsNat _ -> Monads.Error.err WasNat

let rec sem e =
  match e with
  | Val v ->
     return v
  | Eq (a,b) ->
     let* a = sem a in
     let* b = sem b in
     return (IsBool (a=b))
  | Plus (a,b) ->
     let* a = sem a in
     let* a = is_nat a in
     let* b = sem b in
     let* b = is_nat b in
     return (IsNat (a+b))
  | Ifte (i,t,e) ->
     let* i = sem i in
     let* i = is_bool i in
     if i
     then sem t
     else sem e

(** * Tests *)

let%test _ = run (sem (Val (IsNat 42))) = IsNat 42

let%test _ = run (sem (Eq (Val (IsBool true), Val (IsBool true)))) = IsBool true

let%test _ = run (sem (Eq (Val (IsNat 3), Val (IsNat 3)))) = IsBool true

let%test _ =
  run (sem (Eq (Val (IsBool true), Val (IsBool false)))) = IsBool false

let%test _ = run (sem (Eq (Val (IsNat 42), Val (IsNat 3)))) = IsBool false

let%test _ = run (sem (Plus (Val (IsNat 42), Val (IsNat 3)))) = IsNat 45

let%test _ =
  run (sem (Ifte (Val (IsBool true), Val (IsNat 42), Val (IsNat 3)))) = IsNat 42

let%test _ =
  run (sem (Ifte (Val (IsBool false), Val (IsNat 42), Val (IsNat 3)))) = IsNat 3

let%test _ =
  run
    (sem
       (Ifte
          ( Eq (Val (IsNat 21), Plus (Val (IsNat 20), Val (IsNat 1)))
          , Val (IsNat 42)
          , Val (IsNat 3) )))
  = IsNat 42

(** ** Ill-typed expressions *)

let%test _ =
  try
    ignore(run (sem (Plus (Val (IsBool true), Val (IsNat 3)))));
    false
  with
    _ -> true

let%test _ =
  try
    ignore (run (sem (Ifte (Val (IsNat 3), Val (IsNat 42), Val (IsNat 44)))));
    false
  with
    _ -> true
